source "$(dirname $0)/settings.sh"


for project in $(find $WORKSPACE -type d -name ".git")
do
    cd $(dirname $project)
    git pull
done
